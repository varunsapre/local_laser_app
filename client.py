import socketio
from socket import gethostname,gethostbyname
from uuid import getnode
from sys import platform
from datetime import datetime
import calendar,time
import pyautogui as pg
from random import randint


sio = socketio.Client()

@sio.event
def connect():
    print('connection established')
    sio.emit('disconnect')
    
@sio.event
def printm(data):
    data = {'MovementType':"Move","X_Movement":900,"Y_Movement":900,"speed":90}
    sio.emit('movementEvent',data)

@sio.event
def device_details(data):
    device_name = gethostname()
    device_mac = gethostbyname(gethostname())
    device_type = platform
    timestamp = calendar.timegm(time.gmtime())
    deviceWidth = pg.size()[0]
    deviceHeight = pg.size()[1]
    deviceId = randint(100000000000000,999999999999999)
    res = {"deviceName":device_name, "deviceMacID":device_mac, "deviceType":device_type,"deviceConnectedTimeStamp":timestamp,"deviceWidth":deviceWidth,"deviceHeight":deviceHeight,"deviceId":deviceId}
    sio.emit('device_details',res)
    print(data)

@sio.event
def disconnect():
    print('disconnected from server')

sio.connect('http://localhost:8080')
sio.wait()