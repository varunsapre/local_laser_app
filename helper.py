import threading,os,calendar,time,socket,json,pyautogui as pg
from socket import gethostname,gethostbyname
from uuid import getnode
from sys import platform
from datetime import datetime
from random import randint
from pynput.mouse import Button, Controller

def scroll(data):
    
    vsMovement = data['vsMovement']
    hsMovement = data['hsMovement']
    print("scroll",vsMovement,hsMovement)
            
    #if vsMovement != 0:
    x = threading.Thread(target = scroll_thread, args=(vsMovement,hsMovement,))
    x.start()
    #if hsMovement != 0:
    #    x = threading.Thread(target = hscroll_thread, args=(hsMovement,))
    #    x.start()

def move(data):#,mob_h,mob_w):

    dxMovement = data['dxMovement'] #* 100 / float(mob_h)
    dyMovement = data['dyMovement'] #* 100 / float(mob_w)
    #dtMovement =  data['dtMovement'] / 1000
    """siz = pg.size()
    dx = dxMovement * siz[1] / 100
    dy = dyMovement * siz[0] / 100
    pos = pg.position()
    x = dx - pos[1]
    y = dy - pos[0]"""
    #if isinstance(dxMovement, int) and isinstance(dyMovement, int):

    x = threading.Thread(target = move_thread, args=(dxMovement,dyMovement,))
    x.start()
    #x.join(0.00001)

def click(data):
    button = data['button']
    pg.click(button=button)
    
def keypress(data):
    key = data['key']
    pg.press(key)

def keydown(data):
    key = data['key']
    pg.keyDown(key)

def keyup(data):
    key = data['key']
    pg.keyUp(key)

def move_thread(dxMovement,dyMovement):
    pg.FAILSAFE = False
    pg.moveRel(5*dxMovement,5*dyMovement)
    #mouse = Controller()
    #mouse.move(dxMovement, dyMovement)
    
def scroll_thread(vsMovement,hsMovement):
    #pg.vscroll(int(vsMovement))
    mouse = Controller()
    mouse.scroll(hsMovement,vsMovement)

def sendDet():

    device_name = gethostname()
    device_mac = gethostbyname(gethostname())
    device_type = platform
    timestamp = calendar.timegm(time.gmtime())
    deviceWidth = pg.size()[0]
    deviceHeight = pg.size()[1]
    deviceId = randint(100000000000000,999999999999999)
    res = {"deviceName":device_name, "deviceMacID":device_mac, "deviceType":device_type,"deviceConnectedTimeStamp":timestamp,"deviceWidth":deviceWidth,"deviceHeight":deviceHeight,"deviceId":deviceId}
    return res

"""
def mousedown(data):
    button = data['button']
    pg.mouseDown(button=button)

def mouseup(data):
    button = data['button']
    pg.mouseUp(button=button)

def hscroll_thread(hsMovement):
    pg.hscroll(int(hsMovement))
"""