import calendar,os,sqlite3,time
from sqlite3 import Error

def connected_devices():
    connected_devices_list=[]
    conn = None
    database = os.path.realpath('laserapp.db')
    try:
        conn = sqlite3.connect(database)
    except Error as e:
        return e

    q1 = "select device_mac,device_name,device_type from device_details where status = 'active';"

    try:
        cur = conn.cursor()
        res = cur.execute(q1).fetchall()
        for i in res:
            connected_devices_list.append(list(i))

        print(connected_devices_list)
        #if res!=[]:
        #    connected_devices_list.extend(list(res[0]))
    except Error as e:
        return e

    conn.close()
    return connected_devices_list

def storeDB(sid,data):
    
    device_id = data['deviceID']
    device_name = data['deviceName']
    device_mac = data['deviceMacID']
    device_type = data['deviceType']
    device_height = data['deviceHeight']
    device_width = data['deviceWidth']
    wifi_ip = '192.168.69.69'
    status = 'active'
    connect_time = calendar.timegm(time.gmtime())
    disconnect_time = 'NULL'
    
    conn = None
    database = os.path.realpath('laserapp.db')
    try:
        conn = sqlite3.connect(database)
    except Error as e:
        return e

    q1 = "select status from device_details where device_mac = '{}';".format(device_mac)
    q2 = "insert into device_details values('{}',{},'{}','{}','{}',{},{},'{}','{}','{}','{}');".format(sid,device_id,device_name,device_mac,device_type,device_height,device_width,connect_time,disconnect_time,wifi_ip,status)

    #q3 = "update device_details set status = 'active' where (device_mac = '%s');" % device_mac
    #q5 = "select device_mac from device_details where (device_mac = '%s');" % device_mac
    #q5 = "UPDATE device_details SET status = 'active', sid = '%s' WHERE (device_mac = '%s');" % (sid,device_mac)

    try:
        cur = conn.cursor()
        res = cur.execute(q1).fetchall()
        if res==[] or res[0][0] =="inactive": #device_mac does not exist
            cur.execute(q2)
            conn.commit()
        elif res[0][0] == "active": #device_mac already exists
            return "False"
    except Error as e:
        return e

    connected_devices()

    conn.close()
    return None