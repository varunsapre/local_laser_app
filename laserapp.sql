PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE device_details(sid text,device_id integer, device_name text, device_mac text , device_type text, device_height integer, device_width integer,connect_time text,disconnect_time text,wifi_ip text,status text);
COMMIT;
