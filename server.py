import asyncio,socketio,pyautogui as pg
from aiohttp import web
from helper import *
from dbOps import *

sio = socketio.AsyncServer(async_mode='aiohttp',max_http_buffer_size=1000000000000000,logger=True,monitor_clients=False)
app = web.Application()
sio.attach(app)

#async def background_task():
    #Example of how to send server generated events to clients.
    #await sio.emit('device_details', res)
       
#device_det={}

@sio.event
async def connect(sid,environ):
    print('connect ', sid)  
    #await sio.emit('printm')

@sio.event
async def movementEvent(sid,data):
    movementType = data['movementType']
    
    if data['button'] != "none":
        click(data)

    elif movementType == "MoveScroll":        
        scroll(data)

    elif movementType == "Move":
        #mob_h = device_det['deviceHeight']
        #mob_w = device_det['deviceWidth']
        move(data)#,mob_h,mob_w)

    elif movementType == "KeyPress":
        keypress(data)  

    elif movementType == "KeyDown":
        keydown(data)

    elif movementType == "KeyUp":
        keyup(data)

#@sio.on('device_details',namespace='/')
@sio.event
async def device_details(sid,data):
    x = storeDB(sid,data)
    if x=="False":
        await sio.emit('disconnect')
    elif x!=None:
       print(x)
    #global device_det 
    #device_det = data
    res = sendDet()
    await sio.emit('device_details',res)

"""@sio.event
async def disconnect(sid):
    print(sid,'disconnected')
"""
@sio.event
async def disconnect(sid):

    disconnect_time = calendar.timegm(time.gmtime())
    conn = None
    database = os.path.realpath('laserapp.db')
    try:
        conn = sqlite3.connect(database)
    except Error as e:
        return e
    
    q1 = "update device_details set status = 'inactive' where (sid = '{}');".format(sid)
    q2 = "update device_details set disconnect_time = '{}' where sid = '{}';".format(disconnect_time,sid)
    try:
        cur = conn.cursor()
        cur.execute(q1)
        conn.commit()
        cur.execute(q2)
        conn.commit()
        print(sid,'Disconnected')
    except Error as e:
        return e

    conn.close()

if __name__ == '__main__':
    #sio.start_background_task(background_task)
    web.run_app(app,host="0.0.0.0",port=8080)
