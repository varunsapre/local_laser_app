import pyautogui,sys

pyautogui.FAILSAFE = False
height = pyautogui.size()[1]
width = pyautogui.size()[0]
fail_safe_points = [[0,0],[0,height],[width,0],[width,height]]

move = []
x = int(input("Move X to : "))
y = int(input("Move Y to : "))
move.append(x)
move.append(y)

if move in fail_safe_points:
    print("FAIL SAFE ACTIVATED")
    if move[0]==0:
        pyautogui.moveTo(0,move[1])
    elif move[1]==0:
        pyautogui.moveTo(move[0],0)
    else:
        pyautogui.moveTo(width-30,height-30)
else:
    pyautogui.moveTo(move)

pos = pyautogui.position()
print(pos)