import socket
import json
from helper import sendDet
MCAST_GRP = '224.0.0.1'
MCAST_PORT = 5111
# regarding socket.IP_MULTICAST_TTL
# ---------------------------------
# for all packets sent, after two hops on the network the packet will not 
# be re-sent/broadcast (see https://www.tldp.org/HOWTO/Multicast-HOWTO-6.html)
MULTICAST_TTL = 2

res = sendDet()
res=json.dumps(res)
res=res.encode()
res = bytearray(res)
print(res,type(res))
print(res.decode())


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, MULTICAST_TTL)

while True:
    sock.sendto(res, (MCAST_GRP, MCAST_PORT))
