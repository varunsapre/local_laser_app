import sqlite3
from sqlite3 import Error
 
def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn
 
 
def create_table(conn):
    cur = conn.cursor()
    cur.execute("insert into device_details values()")
 
    rows = cur.fetchall()
 
    for row in rows:
        print(row)
 
def main():
    database = r"laserapp.db"
 
    # create a database connection
    conn = create_connection(database)
    with conn:
        create_table(conn)
 
 
if __name__ == '__main__':
    main()